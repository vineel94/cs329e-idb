from flask import Flask, render_template
from get_3books import read_json


app = Flask(__name__)
resources = read_json()

# home page

@app.route('/')
def index():
	return render_template('home.html')



# page for books
@app.route('/books/')
def books():
	return render_template('books.html', book1 = resources[0], book2 = resources[1], book3 = resources[2])

@app.route('/books/book1/')
def book1():
	return render_template('book1.html', book1 = resources[0])

@app.route('/books/book2/')
def book2():
	return render_template('book2.html', book2 = resources[1])

@app.route('/books/book3/')
def book3():
	return render_template('book3.html', book3 = resources[2])



# page for authors
@app.route('/authors/')
def authors():
	return render_template('authors.html', book1 = resources[0], book2 = resources[1], book3 = resources[2])

@app.route('/authors/author1/')
def author1():
	return render_template('author1.html', book1 = resources[0])

@app.route('/authors/author2/')
def author2():
	return render_template('author2.html', book2 = resources[1])

@app.route('/authors/author3/')
def author3():
	return render_template('author3.html', book3 = resources[2])



# page for publishers
@app.route('/publishers/')
def publishers():
	return render_template('publishers.html', book1 = resources[0], book2 = resources[1], book3 = resources[2])

@app.route('/publishers/publisher1/')
def publisher1():
	return render_template('publisher1.html', book1 = resources[0])

@app.route('/publishers/publisher2/')
def publisher2():
	return render_template('publisher2.html', book2 = resources[1])

@app.route('/publishers/publisher3/')
def publisher3():
	return render_template('publisher3.html', book3 = resources[2])



# about us page
@app.route('/about/')
def about():
	return render_template('about.html')



if __name__ == "__main__":
	app.run()
