import json

# primitively reading and organizing book info
# for i in all_book_info: i = [book_info, auth_info, pub_info]
# len(book_info) == 9 if subtitle included, 8 otherwise
def read_json():

	# stores all book info
	all_book_info = []

	with open('3books.json') as json_file:
		books = json.load(json_file)

		# separate into dicts for book_info, auth_info and pub_info, then append a list to all_book_info
		for i in books:


			book_info = {}
			auth_info = {}
			pub_info = {}

			# try and except for book_info		
			for x in range(8):
				try:
					if x == 0:
						book_info['google_id'] = i['google_id']
					elif x == 1:
						book_info['title'] = i['title']
					elif x == 2:
						book_info['isbn'] = i['isbn']
					elif x == 3:
						book_info['publication_date'] = i['publication_date']
					elif x == 4:
						book_info['subtitle'] = i['subtitle']
					elif x == 5:
						book_info['image_url'] = i['image_url']
					elif x == 6:
						book_info['description'] = i['description']
					elif x == 7:
						book_info['genre'] = i['genre']
				except:
					if x == 0:
						book_info['google_id'] = ''
					elif x == 1:
						book_info['title'] = ''
					elif x == 2:
						book_info['isbn'] = ''
					elif x == 3:
						book_info['publication_date'] = ''
					elif x == 4:
						book_info['subtitle'] = ''
					elif x == 5:
						book_info['image_url'] = ''
					elif x == 6:
						book_info['description'] = ''
					elif x == 7:
						book_info['genre'] = ''

			p = i['publishers'][0]
			for x in range(8):
				try:
					if x == 0:
						pub_info['founded'] = p['founded']
					elif x == 1:
						pub_info['name'] = p['name']
					elif x == 2:
						pub_info['location'] = p['location']
					elif x == 3:
						pub_info['wikipedia_url'] = p['wikipedia_url']
					elif x == 4:
						pub_info['description'] = p['description']
					elif x == 5:
						pub_info['parent_company'] = p['parent company']
					elif x == 6:
						pub_info['website'] = p['website']
					elif x == 7:
						pub_info['books'] = p['books']
				except:

					if x == 0:
						pub_info['founded'] = ''
					elif x == 1:
						pub_info['name'] = ''
					elif x == 2:
						pub_info['location'] = ''
					elif x == 3:
						pub_info['wikipedia_url'] = ''
					elif x == 4:
						pub_info['description'] = ''
					elif x == 5:
						pub_info['parent_company'] = ''
					elif x == 6:
						pub_info['website'] = ''
					elif x == 7:
						pub_info['books'] = ''

			a = i['authors'][0]
			for x in range(8):
				try:
					if x == 0:
						auth_info['born'] = a['born']
					elif x == 1:
						auth_info['name'] = a['name']
					elif x == 2:
						auth_info['nationality'] = a['nationality']
					elif x == 3:
						auth_info['description'] = a['description']
					elif x == 4:
						auth_info['wikipedia_url'] = a['wikipedia_url']
					elif x == 5:
						auth_info['image_url'] = a['image_url']
					elif x == 6:
						auth_info['books'] = a['books']
					elif x == 7:
						auth_info['alma_mater'] = a['alma_mater']
				except:
					if x == 0:
						auth_info['born'] = ''
					elif x == 1:
						auth_info['name'] = ''
					elif x == 2:
						auth_info['nationality'] = ''
					elif x == 3:
						auth_info['description'] = ''
					elif x == 4:
						auth_info['wikipedia_url'] = ''
					elif x == 5:
						auth_info['image_url'] = ''
					elif x == 6:
						auth_info['books'] = ''
					elif x == 7:
						auth_info['alma_mater'] = ''


			this_book = [book_info, auth_info, pub_info]
			all_book_info.append(this_book)

	return all_book_info
